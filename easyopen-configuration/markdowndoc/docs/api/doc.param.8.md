
# 参数方式8，重复参数

---


<table>
    <tr>
        <th>接口名</th>
        <td>doc.param.8</td>
        <th>版本号</th>
        <td></td>
    </tr>
</table>

**请求参数**

<table>
    <tr>
        <th>名称</th>
        <th>类型</th>
        <th>是否必须</th>
        <th>最大长度</th>
        <th>示例值</th>
        <th>描述</th>
    </tr>
        <tr><td>goods</td><td class="param-type">object</td><td>否</td><td></td><td><table parentname="goods"><tr><th>名称</th><th>类型</th><th>是否必须</th><th>最大长度</th><th>示例值</th><th>描述</th></tr><tr><td>id</td><td class="param-type">long</td><td>否</td><td>20</td><td>1</td><td>id</td></tr><tr><td>goods_name</td><td class="param-type">string</td><td>否</td><td>64</td><td>apple</td><td>商品名称</td></tr><tr><td>price</td><td class="param-type">float</td><td>否</td><td>11</td><td>7999</td><td>价格</td></tr></table></td><td>goods1</td></tr>
        <tr><td>goods2</td><td class="param-type">object</td><td>否</td><td></td><td><table parentname="goods2"><tr><th>名称</th><th>类型</th><th>是否必须</th><th>最大长度</th><th>示例值</th><th>描述</th></tr><tr><td>id</td><td class="param-type">long</td><td>否</td><td>20</td><td>1</td><td>id</td></tr><tr><td>goods_name</td><td class="param-type">string</td><td>否</td><td>64</td><td>apple</td><td>商品名称</td></tr><tr><td>price</td><td class="param-type">float</td><td>否</td><td>11</td><td>7999</td><td>价格</td></tr></table></td><td>goods2</td></tr>
        <tr><td>goods3</td><td class="param-type">object</td><td>否</td><td></td><td><table parentname="goods3"><tr><th>名称</th><th>类型</th><th>是否必须</th><th>最大长度</th><th>示例值</th><th>描述</th></tr><tr><td>id</td><td class="param-type">long</td><td>否</td><td>20</td><td>1</td><td>id</td></tr><tr><td>goods_name</td><td class="param-type">string</td><td>否</td><td>64</td><td>apple</td><td>商品名称</td></tr><tr><td>price</td><td class="param-type">float</td><td>否</td><td>11</td><td>7999</td><td>价格</td></tr></table></td><td>goods3</td></tr>
        <tr><td>goods4</td><td class="param-type">object</td><td>否</td><td></td><td><table parentname="goods4"><tr><th>名称</th><th>类型</th><th>是否必须</th><th>最大长度</th><th>示例值</th><th>描述</th></tr><tr><td>id</td><td class="param-type">long</td><td>否</td><td>20</td><td>1</td><td>id</td></tr><tr><td>goods_name</td><td class="param-type">string</td><td>否</td><td>64</td><td>apple</td><td>商品名称</td></tr><tr><td>price</td><td class="param-type">float</td><td>否</td><td>11</td><td>7999</td><td>价格</td></tr></table></td><td>goods4</td></tr>
    </table>

**参数示例**

```json
{
	"goods":{
		"goods_name":"apple",
		"price":"7999",
		"id":"1"
	},
	"goods4":{
		"goods_name":"apple",
		"price":"7999",
		"id":"1"
	},
	"goods3":{
		"goods_name":"apple",
		"price":"7999",
		"id":"1"
	},
	"goods2":{
		"goods_name":"apple",
		"price":"7999",
		"id":"1"
	}
}
```

**返回结果**

<table>
    <tr>
        <th>名称</th>
        <th>类型</th>
        <th>描述</th>
    </tr>
    <tr>
        <td>code</td>
        <td>string</td>
        <td>状态值，"0"表示成功，其它都是失败</td>
    </tr>
    <tr>
        <td>msg</td>
        <td>string</td>
        <td>错误信息，出错时显示</td>
    </tr>
        <tr>
        <td>data</td>
        <td>object</td>
        <td>返回的数据，没有则返回{}
            <table>
                <tr>
                    <th>名称</th>
                    <th>类型</th>
                    <th>最大长度</th>
                    <th>示例值</th>
                    <th>描述</th>
                </tr>
                            </table>
        </td>
    </tr>
    </table>

**返回示例**

```json
{
	"code":"0",
	"data":{},
	"msg":""
}
```


